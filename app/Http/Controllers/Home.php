<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use PDO;

class Home extends Controller
{
    public function index()
    {
        $id = 1;
        $pdo = DB::connection()->getPdo();
        $sql = 'select id, name from users where id = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $name = $result[0]["name"];
        //var_dump($result[0]["name"]);
        return view('home', compact('name'));
    }
}
