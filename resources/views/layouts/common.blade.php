<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | ghosipp!</title>
    <meta name="description" itemprop="description" content="@yield('description')">
    <meta name="keywords" itemprop="keywords" content="@yield('keywords')">
    <link href="/css/app.css" rel="stylesheet">
    @yield('pageCss')
</head>
<body>
    <header class="header">
        <div class="header_menu bg_color_red">ヘッダー</div>
    </header>
    <div class="contents">
        <div class="main">
            @yield('content')
        </div>
    </div>
    <footer class="footer">
        <div class="header_menu bg_color_blue">フッター</div>
    </footer>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('pageJs')
</body>
</html>