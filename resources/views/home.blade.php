<!-- resources/view/home.blade.php -->
@extends('layouts.common')
@section('title', 'ゴシップ！')
@section('keywords', 'ゴシップ,記事,体験談')
@section('description', '情報の価値を知る')

@section('pageCss')
@endsection
 
@section('content') 
    <div class="dropdown">
        <button type="button" id="dropdown1"
            class="btn btn-secondary dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">
            Dropdown button {{$name}}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdown1">
            <a class="dropdown-item" href="#">Menu #1</a>
            <a class="dropdown-item" href="#">Menu #2</a>
            <a class="dropdown-item" href="#">Menu #3</a>
        </div>
    </div>
@endsection

@section('pageJs')
<a href="/Top">google.com</a>
@endsection
 
